'use strict';

(function() {
  selectActiveMainMenu();
  document.app = {};
  document.app.quotePopup = new QuotePopup();
  openQuotePopupOnRefsInText();
  activateGoogleAnalytics();
})();

function activateGoogleAnalytics() {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59762004-2', 'auto');
  ga('send', 'pageview');

  let linkIdsToTrack = ['karma-marketplace-link', 'java-course-link', 'datagen-link'];
  for(let i = 0; i < linkIdsToTrack.length; i++) {
    let id = linkIdsToTrack[i];
    let link = document.getElementById(id);
    if (link) {
      (function(id){
        addListener(link, 'click', function() {
          ga('send', 'event', 'button', 'click', id);
        });
      })(id);
    }
  }
}

function selectActiveMainMenu() {
  let menus = document.getElementsByName('main-menu-item');
  for (let i = 0; i < menus.length; i++) {
    let menu = menus[i];
    if (document.URL.indexOf(menu.href.split(".html")[0]) !== -1) {
      menu.className = menu.className + " active";
    }
  }
}
function openQuotePopupOnRefsInText() {
  const links = document.getElementsByClassName('data-quote-link');
  for (let i = 0; i < links.length; i++) {
    links[i].textContent = i + 1;
    links[i].addEventListener('click', function(e) {
      e.preventDefault();
      const href = e.target.attributes.href.value;
      const quoteContainer = document.getElementById(href.slice(1));
      document.app.quotePopup.show(quoteContainer, e.target);
    });
  }
}

function QuotePopup () {
  self = this;
  const quoteModal = document.getElementById('quote-modal');
  const quoteModalContent = document.getElementById('quote-modal-content');
  _initPopupListeners(this.quoteModal);

  /** @param quoteContainer {HTMLElement} contains HTML to put into the popup */
  this.show = function(quoteContainer) {
    quoteModalContent.innerHTML = quoteContainer.innerHTML;
    quoteModal.style.display = 'block';
    quoteModal.focus();
  };
  this.close = function() {
    quoteModal.style.display = 'none';
  };

  function _initPopupListeners(quoteModalContainer) {
    const quoteModalClose = document.getElementById('quote-modal-close');
    if(quoteModalClose) {//there could be no quote modal on current page at all
      quoteModalClose.addEventListener('click', function(e) {
        e.preventDefault();
        self.close();
      });
      document.addEventListener('keydown', function(e) {
        if((e.key==='Escape' || e.key==='Esc' || e.keyCode===27)) self.close();
      });
    }
  }
}

/**
 * Utility to wrap the different behaviors between W3C-compliant browsers
 * and IE when adding event handlers.
 *
 * @param {Object} element Object on which to attach the event listener.
 * @param {string} type A string representing the event type to listen for
 *     (e.g. load, click, etc.).
 * @param {function()} callback The function that receives the notification.
 */
function addListener(element, type, callback) {
  if (element.addEventListener) element.addEventListener(type, callback);
  else if (element.attachEvent) element.attachEvent('on' + type, callback);
}