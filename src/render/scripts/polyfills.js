'use strict';

(function(){
  addHiddenSupportToIE();
})();
function addHiddenSupportToIE() {
  if (document.documentElement.hidden === undefined) {
    Object.defineProperty(Element.prototype, 'hidden', {
      set: function(value) {
        this.setAttribute('hidden', value);
      },
      get: function() {
        return this.getAttribute('hidden');
      }
    });
  }
}