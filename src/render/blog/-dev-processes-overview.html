---
title: "Kanban vs. Just-in-Time vs. Lean"
layout: "blogEntry"
isPage: true
---

<div class="blog-container">
  <div>
    <div class="blog-main">
      <div class="blog-post">
        <h1 class="blog-post-title">Overview of modern dev processes</h1>

        <p class="blog-post-meta">Jun 3, 2020 by Stanislav Bashkyrtsev</p>
        <ul>
          <li>Are you convinced that Scrum is an Agile methodology?</li>
          <li>You keep hearing about Lean and Kanban, but don't understand the difference?</li>
          <li>You have DevOps engineers in your company?</li>
        </ul>
        <p>You're not alone. There're surprisingly many people in Software Development who are misinformed in
          regards of development approaches and ideologies. So let's go over all the buzzwords.
          Each topic discussed here deserves a separate article or a book, the goal of this post is to give
          an overview. To explain subtleties to those who already have read or heard about these processes. We'll cover
          Waterfall, Scrum, Agile, Kanban, Just-in-Time (JiT), Lean, Theory of Constraints (ToC),
          Continuous Delivery (CD), Continuous Integration (CI), DevOps.</p>

        <h2>Iterative approaches: Waterfall, Scrum</h2>
        <p>Many people put Waterfall and Scrum into opposite corners. Like they are the opposites. But really
        they aren't that different because both of them are <i>iterative</i>. Meaning that:</p>
        <ol>
            <li>You plan a set of features</li>
            <li>Then you implement, test, release them</li>
            <li>And you repeat</li>
        </ol>
        <p>Some people think that Waterfall doesn't have iterations, that you plan, code and test once
          during the lifespan of the project. While most likely history has witnessed such cases, more generally we call Waterfall something
          that has very long iterations (several months). And in this regard the difference between Scrum and Waterfall
          becomes quantitative (duration of iteration) rather than qualitative. It doesn't mean that this is a small
          difference though - duration of the cycle has <b>huge</b> impact on the process.</p>
        <h2>Scrum vs. Agile</h2>
        <p>The reason Agile and Scrum are connected is that the creators of Scrum (Ken Schwaber, Jeff Sutherland)
          participated in signing <a href="https://agilemanifesto.org/">Agile manifesto</a>. This was an awesome
          marketing move, but it didn't make Scrum any more agile. To summarize the difference:</p>
        <ul>
          <li>Agile software development is when teams decide which process suits them and change it accordingly.
            In other words being agile means having common sense and doing what's best for your project.</li>
          <li>Scrum is just one of the processes</li>
        </ul>
        <p>So if you decided to change your process to Scrum because it makes you more effective - then you're agile.
          If you decided to replace Scrum with something else which you deem is even more effective, then you're also agile. But if you stick
          with Scrum even though you see how ineffective it is for you - then you're <b>not</b> agile. Note, that
          you may even choose Waterfall because it's best for your situation - and you still will be agile.</p>

        <h2>Theory of Constraints</h2>
        <p>If you haven't read <a href="https://en.wikipedia.org/wiki/The_Goal_(novel)">The Goal</a> and
          <a href="https://en.wikipedia.org/wiki/Goal_II:_Living_the_Dream">Goal II</a> - you must do it. These
          books open your eyes on what is an effective process. They are applicable to manufacturing,
          projects, business, life, societies, everything! The key idea is that when you have a sequence of steps
          - one of them will be slower than others. Even if you have the same number of people, even if you
          have robots instead of people - the speed will always be different. And it's this slowest step that's most
        important as it dictates the speed of the whole system - such step is called a Constraint.</p>
        <p>Constraints are not a bad thing - they always exist. But knowing where constraint is in the process
          makes you understand where you should and shouldn't make improvements. ToC has a limited use in projects -
          but it's still very useful because even in projects you have sequential steps. In IT it could be:
          Business Analysts -> Developers -> Testers. Which of these require more man power? You'll have to read
          the books, but I'll give some insights into ToC applications in software projects.</p>
        <p>ToC has 2 main metrics: </p>
        <ul>
          <li>Throughput - how many tasks you finish (release) in a unit of time. It's important to measure it over
            short periods. So if you release 1 feature every day you have high throughput, but if you release 365
            features in a year - the throughput is low, and that's bad.</li>
          <li>Inventory Costs a.k.a. Unfinished Work - how much work has been done but isn't sold (released) yet. This is
            usually the metric people forget about, so it's the one that you need concentrate at.</li>
        </ul>
        <p>Teams that follow Theory of Constraints:</p>
        <ul>
          <li>Release every feature separately or in small batches</li>
          <li>Do not have a lot of parallel work (e.g. 10 developers working on 20 tasks)</li>
          <li>Do not do a lot of upfront work (product backlog for next 12 months)</li>
        </ul>
        <h2>Just-in-Time (Pull)</h2>
        <p>JiT is very similar to ToC, but instead of following the pace of the slowest step we
          follow the pace of the subsequent step. So:</p>
        <ol>
          <li>Step1 finished 5 items, but people from Step2 are overloaded and can't take the new work yet</li>
          <li>Step1 stops working until Step2 can take some items</li>
          <li>Once Step2 is able to take 1 item, people at Step1 start working on 1 new item</li>
        </ol>
        <p>That's why it's also called a Pull system - Step2 pulls the tasks from Step1. And until that happens Step1
          does nothing.</p>
        <p>When the chain of steps is short the difference between Theory of Constraints and Just-in-Time becomes
          even more vague. It's even possible (and I would recommend that) to make them equal - by making the 1st
          step the slowest in the system. E.g. if you have 2 steps - Developers and Testers -
          <b>add redundancy in testing</b>. This way Developers can give the tasks to Testers right away w/o the
          need to wait. This has a lot of positive effects on the team.</p>
        <h2>Lean</h2>
        <p>Unfortunately Lean wasn't introduced as a separate term but rather as a comparison to Ford's Mass Production.
          Thus it's hard to talk about Lean separately, so let's dive a bit into history. At the beginning of XX
          century Henry Ford introduced first Mass Production in automotive industry (previously there were small shops
          that created individual cars for individual clients). The idea is to have an assembly line - people
          stand one after another, car parts are moving from the beginning to the end of the line, and people do a very
          small task with that part. But after some years several problems manifested themselves:</p>
        <ul>
          <li>Because each person has a very small responsibility they didn't know anything else. And thus if a
            broken part was passing buy they couldn't tell that it's broken and would keep doing whatever they were
            asked. This meant that whole assembly line would spend time and material on something that
            should've been tossed away.</li>
          <li>This also meant that these people could not suggest improvements to the process.</li>
          <li>Also, at some point the market saturated, but the production of cars didn't stop (interruptions were expensive)
            and kept producing new cars even though there were no buyers.</li>
        </ul>
        <p>Toyota on the other hand was a poor company at the time, it didn't have a luxury of being so wasteful.
          They worked on their process to make it lean (meaning thin, with no overproduction, not a lot of extra space
          to keep spare parts, etc). They kept improving it until they could quickly react to the market
          (stop producing PartA when it's not needed and swiftly start producing it again when the need suddenly arises).</p>
        <p>So Lean is all about continuous improvement of the process, decreasing the amount of waste in it,
          being agile, having transparent process that people understand and can impact. That's philosophical part
          of it. The more practical part is Just-in-Time. So take the philosophy of continuous improvement, put it
          together with JiT - you got yourself Lean.</p>
        <h2>Kanban vs. Just-in-Time</h2>
        <p>In the 1st edition of The Machine That Changed The World (written in 1991) when describing how Toyota
          worked they made some mistakes in terminology. They thought that Kanban is the same thing as JiT.
          But in the 2nd edition (released in 2007) they apologized and fixed the terminology. Kanban means "card"
          and nothing more. So when you have a board of with Stories and they look like cards - you can call them
          kanban (if you wanted to be the weird one in the team).</p>
        <p>In IT the introduction of this term came with a nice book by David J. Anderson called
          <a href="https://www.amazon.com/Kanban-Successful-Evolutionary-Technology-Business/dp/0984521402">Kanban - Successful Evolutionary Change for Your Technology Business</a>.
          While it was written in 2010, the ideas were born much earlier. At that time only the 1st edition of The Machine
          existed so people started to use JiT and Kanban interchangeably.
        </p>
        <p>So <b>Kanban in IT is just a mistake</b>, whenever people say Kanban they mean Just-in-Time.</p>
        <h2>Continuous Delivery</h2>
        <p>So there are 3 types of processes common these days:</p>
        <ul>
          <li>Iterative (Waterfall, Scrum): they are obsolete and most likely don't have a lot of use cases.</li>
          <li>For the lack of a better term.. Sequential (Theory of Constraints, Just-in-Time): once you finish a task
            - release it. It's much faster than iterative processes and results in higher quality.</li>
          <li>Continuous Delivery: even if your task isn't finished - release it.</li>
        </ul>
        <p>While in real world we can't give the product while it's not finished, in the virtual world of
          Software Development we can do even better. If you started to work on a task and you have changes that
          aren't yet finished you can still deploy them to production. Of course you need to hide them and there are
        techniques like Branching by Abstraction, Feature Toggles that can help you with that. When you finish the
          task the only thing that's left is to unhide it and you're done.</p>
        <p>There are many reasons to do this, but they can all be summed up with 1 metric - Inventory Costs. You
          decrease Unfinished Work because you don't wait for the feature to be ready to release it. This speeds up
          the process and lowers the risks (the smaller the changes are the less likely they would lead to big
          problems).</p>
        <p>Will CD work for everyone? Nope. CD requires good development skills and discipline. But the bigger and
          older the project is the harder it is to survive w/o CD. So when comparing with others:</p>
        <ul>
          <li>If you dev team can write high quality code with small number of bugs CD will speed you up a lot.</li>
          <li>If the code quality is low CD may result in unnoticed bugs because developers push code to hidden
            features which aren't checked by testers. And when they do so they may touch existing features. But
            testers won't know that things need to be re-tested.</li>
          <li>You may find a good solution in between. E.g. if developers are sure their changes are safe -
            they push them and release them with other features. If changes are more complicated - they wait
            (Pull system) until Testers are ready to test.</li>
        </ul>
        <h2>Continuous Integration</h2>
        <p>CI is a culture in which all team members are trying to integrate their changes together. There are 2
        opposites:</p>
        <ol>
          <li>Developers working in long-running branches for weeks or months and then merging with the main
            branch (trunk, master). Such process leads to painful merges and a lot of bugs after that.</li>
          <li>Developers using only 1 branch and constantly pushing all the changes there (trunk-based development).
            The ultimate Continuous Integration. Because you integrate your code with the rest of the team you
            constantly check it all working together and thus the likelihood of big surprises diminishes.</li>
        </ol>
        <p>There's plenty of space in between: some people stash their changes for a day in branches, others may
          keep branches for a week. But the longer you keep them the greater the chance that when combined the code
        won't work. So keep your Inventory Costs low and merge your changes to the trunk frequently.</p>
        <p>Continuous Delivery depends on Continuous Integration. You can barely implement CD without CI and thus
        people often refer them as CI/CD or CD & CD.</p>
        <h2>DevOps</h2>
        <p>It's very common to hear people referring to themselves or to others as DevOps. But there is no such role!
        What they are actually referring to is.. well, simply Ops or Admins - people who automate deployments,
          maintain servers and configuration.</p>
        <p>What this term actually means is culture in which teams work together to achieve common goals instead of
          working for their own metrics and achievements. The title alludes to Development & Operations
          teams/departments working together, but really it means everyone - Testers, DBAs, Business collaborating.
        </p>
        <p>The opposite of DevOps is companies divided into functional departments (Dev, DBA, Testing, Business, etc)
          each having its own director not willing to collaborate and each working for their own good. Because
          everyone has their own KPIs they try to optimize their own work. In Theory of Constraints this is called a
          Local Optimum. Problem is - these optimizations harm overall progress. When people from DepartmentA
          can sacrifice their own performance to help DepartmentB because it's better for the company as a whole
          - that's what we call DevOps. This way we're trying to achieve Global Optimum.</p>
      </div>
