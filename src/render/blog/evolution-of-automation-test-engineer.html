---
title: "Evolution of Automation Test Engineer"
layout: "blogEntry"
isPage: true
---

<div class="blog-container">
  <div>
    <div class="blog-main">
      <div class="blog-post">
        <h1 class="blog-post-title">Evolution of Automation Test Engineer</h1>

        <p class="blog-post-meta">February 7, 2015 by Stanislav Bashkyrtsev | Last Edited: 8 November, 2015</p>

        <p>When we’re new to a tool, we usually start using this tool in a wrong way. But experience & time
          changes the way we think of it, we start using it differently. Here I’d like to show what stages I personally
          went through while writing System & Component tests until I understood exactly how testing should happen in an
          average web project. Many of others go through similar steps, maybe this post will help speed up their personal
          evolution.</p>

        <h2>Homo habilis: Mixing test cases & tools</h2>

        <p>So here we are - started exploring tools like Selenium. Our test cases first look like this:</p>
        <pre><code class="language-java">public void clickSignUpLinkTest(String appURL) {
  driver.get(appURL);
  driver.findElement(By.xpath("//a[@href='/jcommune/user/new']")).click();
  driver.findElement(By.id("password")).clear();
  driver.findElement(By.id("passwordConfirm")).clear();
  driver.findElement(By.id("password")).sendKeys(acceptedPassword);
  driver.findElement(By.id("passwordConfirm")).sendKeys(acceptedPassword);
  driver.findElement(By.xpath("//button[@type='submit']")).click();
  assertEquals(driver.getCurrentUrl(), appURL);
}</code></pre>
        <p>People usually don’t stay on this stage for more than several months because it clearly sucks.</p>
        Pros:
        <ul>
          <li>Fast to write and thus might be a good starting point which afterwards gets refactored into something more
            solid.
          </li>
          <li>Fast to write and thus can be more cost-effective if the functionality under test is experimental and the
            odds of refactoring are high.
          </li>
        </ul>
        Cons:
        <ul>
          <li>Not always clear what test case does since it’s mixed with low-level logic of Selenium</li>
          <li>If technical part changes (e.g. selectors), test case must be changed even though it didn’t change per
            se
          </li>
          <li>Test cases may become very large</li>
        </ul>
        <h2>Homo erectus: Using Page Objects</h2>

        <p>We started to understand that selectors should move out into separate objects to get rid of the mess we’ve
          written so far. At this stage we have a Page class:</p>
        <pre><code class="language-java">public class SignInPage {
  @FindBy(id = "j_username")
  WebElement usernameField;

  public WebElement getUsernameField() { return usernameField; }
}</code></pre>
        <p>And we use it in the test cases:</p>
        <pre><code class="language-java">public void usernameValidationTest(String username) {
  signInPage.getUsernameField().sendKeys(username);
  signInPage.getSubmitButton().click();
  assertExistBySelector(driver, signInPage.errorMessageSel);
}</code></pre>
        <p>Now test cases are not blotted with stuff like selectors. Some actions like <code>click()</code> may also
          move to Page Object.</p>
        Pros:
        <ul>
          <li>It’s more clear what the test case does step by step</li>
          <li>If selector changes, we don’t change the test case itself</li>
        </ul>
        Cons:
        <ul>
          <li>Test case is still prone to changes because it’s page-oriented. What if previously we were moving from
            PageA to PageB, but after a change we started to move from PageC to PageB? We would need to change both
            test case and Page Object while test case that checks the validity of some field on page didn’t really
            change.
          </li>
          <li>We would need different test cases for actions achieved through different paths. Imagine that your pages
            are adaptive to screen sizes: on large screens your elements differ from those shown on small screens.
            In page-oriented test cases you’ll have to write different tests for different devices while the case
            doesn’t differ.
          </li>
          <li>The test cases can still be pretty large depending on the complexity of the workflow. Dependency is
            linear.
          </li>
        </ul>
        This is where most of AQA get stuck. It’s what’s shown in most of tutorials. It’s what most of framework authors
        want you to do. Now you may start worrying since we’re only half-way through :)
        <h2>Homo antecessor: Business-Oriented Test Cases</h2>
        Since we figured out what to improve in Page Oriented Test Cases, let’s see how this can be refactored. We would
        still need Page Objects:
        <pre><code class="language-java">public class SignInPage {
  @FindBy(id = "j_username")
  WebElement usernameField;

  public WebElement getUsernameField() { return usernameField; }
}</code></pre>
        But now in Test Cases we use business operations that don’t care about pages:
        <pre><code class="language-java">public void usernameAsSpace_shouldFail() throws Exception {
  UserForRegistration user = UserForRegistration.withUsername(" ");
  Users.signUp(user);
}</code></pre>
        And we’ve created another layer that implements business operations:
        <pre><code class="language-java">public static User signUp(UserForRegistration userForRegistration) throws ValidationException {
  mainPage.logOutIfLoggedIn(driver);
  openAndFillSignUpDialog(userForRegistration);
  checkFormValidation(signUpPage.getErrorFormElements());
  signUpPage.closeRegistrationWasSuccessfulDialog();
  return userForRegistration;
}</code></pre>
        Pros:
        <ul>
          <li>Test cases are very compact - their complexity doesn’t quite depend on the complexity of the workflow
            since it’s hidden behind the business layer.
          </li>
          <li>It’s very easy to read such cases even for not-quite-technical people.</li>
          <li>Test cases are fully unbound from pages and technical details. No matter what changes, if Sign Up
            operation still exists in the app, test case won’t change unless the rules of the Sign Up functionality
            change.
          </li>
        </ul>
        Cons: at this point you feel like everything is done in the best possible way - cases are very concise and
        readable, who knows, maybe they are finally stable. But most probably you still have problems (which could’ve
        been present on previous stages of your evolution):

        <ul>
          <li>Test runs take too long. At <a href="http://jtalks.org/">JTalks</a> after we optimized tests, split some
            of them to run in headless mode (HtmlUnit) they still took about 5 hours to finish. Usually it’s not a big
            deal because tasks are being done continuously and QA always have tasks if Devs work fast enough. But
            sometimes it becomes a real drag especially if you want to release a hot fix.
          </li>
          <li>Because of the large number of tests in a single run (we had 100 cases in 4 browsers + 300 headless tests
            = 700 tests per one run) they will still fail no matter what. At <a href="http://jtalks.org/">JTalks</a>
            this wasn’t often, we could have dozens of green runs, but then out of blue one test would fail. Before we
            split them into headless & browser tests they were failing much more often, but that still would happen even
            after the split.
            I also asked one of Selenium Devs whether their tests (specifically to check if Selenium works) fail without
            real reasons. And it appeared they do. Very seldom, but this happens. And I think that they fail rarely only
            because the pages they use are very lightweight and simplistic. You wouldn’t have this luxury “in the wild”.
          </li>
        </ul>
        As I said - these problems show up no matter on what level of evolution you are, but then we finally are coming
        closer to nirvana.
        <h2>Homo sapiens: Writing Component Tests</h2>
        Do you remember those Test Pyramids where you have Unit Tests at the bottom, then Component Tests (a.k.a.
        Subcutaneous Tests) and only after that System Tests? They do make sense. I don’t know why so many people forget
        about them and write only Unit & System Tests. So here is the bottom line:
        <ul>
          <li>There should be a small number of System Tests.</li>
          <li>Profit!</li>
        </ul>
        Here is how we do it:
        <ul>
          <li>System Tests check only UI and no business logic. Let’s say you have a Sign In form and there are a lot of
            validation rules. In System Tests we only need to check situations that differ on UI. E.g. Happy Path and 1
            validation error. We don’t need to test several Happy Paths like Lower Boundary, Upper Boundary, Special
            Symbols. One successful case and one negative case should be enough (well, if there are differences in UI
            for different negative cases, then it might make sense to test them as well).
          </li>
          <li>Now Component and Unit Tests check business logic: things like validation (all negative cases),
            all the equivalence classes, special symbols, etc.
          </li>
        </ul>
        Pros:
        <ul>
          <li>Component Tests are way faster than System Tests. You’ll decrease the number of System Tests, ergo your
            lead time will be smaller.
          </li>
          <li>Devs can even run Component Tests on their local machines. Very fast feedback.</li>
          <li>You won’t need to bother too much with stability - because the number of System Tests will be kept low,
            spontaneous failures will be rare.
          </li>
        </ul>
        <h2>How to implement Component Tests</h2>
        Different eco systems will require different setups, but here are several examples:
        <ul>
          <li>Spring MVC apps may leverage MockMvc framework to emulate requests from users.</li>
          <li>Apps that use some HTTP based protocol (REST/SOAP via HTTP) may use libs like <a
              href="https://code.google.com/p/rest-assured/">REST-assured</a>, <a
              href="https://github.com/mkotsur/restito">Restito</a>. Additionally frameworks like RestEasy may provide
            functionality to invoke methods of services directly
            <sup>[<a href="https://docs.jboss.org/resteasy/docs/1.1.GA/userguide/html/RESTEasy_Server-side_Mock_Framework.html">doc</a>]</sup>
            (without HTTP).
          </li>

          <li>Those who use MOMs like IBM MQ, Tibco EMS, Solace may want to use ActiveMQ as an alternative for Component
            Tests. Well, Devs who work with MOM usually write Component Tests anyway, so they know how to do it.
            Although they usually don’t implement them in a readable way :) If Apache Camel or Spring Integration is
            used, then we can use testing capabilities of those frameworks.
          </li>
        </ul>
        <p>It’s important to realize that you don’t have to bring up the whole App in order to run Component Tests - you
          may simply initialize what you need for a particular case.</p>

        <p>For such tests it’s usually preferred to use in-memory databases. There are tools that help implementing
          this: ORMs and DBs that can mimic other DBs (HSQLDB, TimesTen).</p>

        <h2>Should AQA be involved?</h2>

        <p>My position on this - all the tests, no matter what level, have to be maintained by Devs. Manual QA may help
          coming up with test cases, they can also review existing tests (you have to write them in a readable form to
          let this happen) or even write tests themselves if you provide an easy-to-use business-oriented framework. AQA
          are more expensive but in the same time they won’t be able to implement business layers of the tests without
          Devs.</p>

        <p>You may still keep AQA for Selenium stuff, but their results have to be carefully reviewed by Devs.</p>

        <h2>Wrapping Up</h2>

        <p>To keep tests fast, reliable and easy to maintain you need something more than just doing selenium right.
          It's architecture and the whole approach that should be effective, not the tests per se. And this leads
          us to a Test Pyramid. Checkout out our next article to see an example of how to implement all 3 levels of
          tests in practice:
          <a href="http://qala.io/blog/test-pyramid.html">Building Test Pyramid to optimize automated testing</a>.
        </p>

      </div><!-- /.blog-post -->

    </div><!-- /.blog-main -->


  </div><!-- /.row -->

</div>

<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "BlogPosting",
      "headline": "Evolution of Automation Test Engineer | Qala",
      "datePublished": "2015-02-07T00:00:00+00:00",
      "description": "Moving from low-grade System Tests that mix Selenium and test cases to Page Objects, Business Layers and Component Tests that are robust and fast"
    }
</script>