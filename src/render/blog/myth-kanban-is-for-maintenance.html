---
title: "Busting myth about Kanban suitable for support projects only"
layout: "blogEntry"
isPage: true
---

<div class="blog-container">
  <div>
    <div class="blog-main">
      <div class="blog-post">
        <h1 class="blog-post-title">Busting myth about Kanban suited for maintenance only</h1>

        <p class="blog-post-meta">Jun 16, 2020 by Stanislav Bashkyrtsev</p>
        <p>It's commonly accepted in IT management circles that <del><a href="lean-jit-kanban.html">Kanban</a></del>
          Just-in-time is suited mainly for projects past active
          development. But it seems that most people simply repeat this statement without giving it a good thought -
          why is this true, <i>is</i> it actually true? I'd like to explain how this statement came to life, and
          show that it's not only possible - it's <i>easy</i> to apply JIT to projects that are being actively
          developed.</p>

        <h2>Origin of the myth</h2>
        <p>The idea that JIT mainly suits maintenance-stage projects was born in book
          <a href="https://www.amazon.com/Kanban-Successful-Evolutionary-Technology-Business/dp/0984521402">Kanban - Successful Evolutionary Change for Your Technology Business</a>
          written by <i>David J. Anderson</i>. According to the book the process mainly boils down to such algorithm:
        </p>
        <ul>
          <li>StepA finishes a task and puts it into StepB buffer</li>
          <li>If StepA works faster than StepB the buffer fills up (let's say it has 5 slots), after that StepA <i>has to stop</i></li>
          <li>Once StepB takes a task from the buffer a slot frees up. This unblocks StepA until the buffer is filled up again.</li>
        </ul>
        <h3 style="width:270px; margin-left: auto; margin-right: auto; display: block;">Just-in-time: expectations</h3>
        <video playsinline autoplay muted loop controls>
          <source src="/img/jit-expectations.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>
        <p>Simple in theory, but here is a problem that the author ran into:</p>
        <ul>
          <li>Suppose that the buffer is full: StepB works on <i>complicated</i> tasks at the moment and blocks StepA for a full week</li>
          <li>The <i>tasks in the buffer are simple</i>, so once StepB is free to work on them - it finishes all of them in 1 day</li>
          <li>This unblocks StepA and people start working on new tasks - but those are complicated and will take a week</li>
        </ul>
        <p>
          As a result StepA was idle for a week, and then StepB becomes idle for another week. So we just lost a week -
          if StepA kept working it would've provided new tasks to StepB immediately. Oops.
        </p>
        <h3 style="width:200px; margin-left: auto; margin-right: auto; display: block;">Just-in-time: reality</h3>
        <video playsinline autoplay muted loop controls>
          <source src="/img/jit-reality2.mp4" type="video/mp4">
          Your browser does not support the video tag.
        </video>
        <p>The root of the problem lies in how we measure buffer size. In our case we capped it at 5 tasks, but
          the tasks were very different in duration. If all the tasks were of the same duration both steps would've
          synchronized. They would not be <i>both</i> idle <a class="data-quote-link" href="#one-step-is-always-slower">ref</a>.</p>
        <p>Therefore the book says that we should do our best to standardize the tasks in terms of duration. And because
          maintenance tasks are usually easier to standardize and they are more predictable the outcome is:
          JIT is bad for projects in active development and is good for support phase.</p>

        <h2>Fix #1 - change units of the buffer</h2>
        <p>The natural way of capping the buffer is by counting the tasks, but is it the best way?
          We'd have to standardize the tasks to make it work, but this is a very (very!) complicated problem.
          It requires us to estimate with high level of precision, and then split or combine tasks if they are not
          standard. This is a lot of work even for support-phase projects.</p>
        <p>Instead we could measure the buffer in hours/points/whatever you estimate in. This way the tasks
          could be of different sizes and the max number of tasks in the buffer can also be different. In our case
          above this would allow to finish not 5 but say.. 8 tasks and only then halt StepA. If you want
          to be more accurate you'd want to estimate tasks for StepA and StepB <i>separately</i> (some
          tasks may be simple for StepA and complicated for StepB or vice versa), but this is more time consuming.</p>
        <p>Also instead of estimating you can simply employ your gut feeling. Usually team leaders understand
          which task is complicated and risky and which one is simple. Such person can decide
          whether we want new tasks to be pulled out of backlog or we should wait.</p>

        <h2>Fix #2 - move the constraint upstream</h2>
        <p>Adding extra Test engineers or removing some of the Dev engineers is a simple and beautiful solution
          to the problem. But in order to understand how this helps you need some background in the Theory of
          Constraints... The difference between JIT and ToC is pretty subtle:</p>
        <ul>
          <li>JIT tries to optimize the whole process. It suggests that each <i>subsequent</i> step controls the amount
            of work <i>previous</i> step can do. And at some point we'd like to balance the performance of all steps.</li>
          <li>ToC recognizes that in each system there's always a bottleneck (constraint). This isn't bad and we shouldn't
            necessarily try to eliminate the constraint <a class="data-quote-link" href="#constraints-forever">ref</a>.
            Instead we should <i>use</i> the constraint to set the rhythm of the <i>whole system</i>.</li>
        </ul>
        <p>From ToC perspective no steps in the system must work at 100% except for the
          slowest step - this step defines the <i>overall</i> performance. And if other steps also work
          at 100% then they produce work that can't be consumed by the constraint timely and thus Inventory Costs
          (unfinished work) grow. Which in turn slows down the whole system.</p>
        <p>In systems where constraint is loosely defined and can migrate from step to step it's hard to
          figure out which of the steps is the most precious. In such cases it's too easy to start wasting these precious
          resources on some unimportant work that could've been done (or prevented) by others.</p>
        <p>So the constraint must exist and it's good if we know it. Unfortunately things are more complicated if
          the constraint is in the middle or at the end of the process as we somehow should notify all prior
          steps that they need to work more (or less). But if we put the constraint <i>at the very beginning</i>
          - no additional orchestration is necessary. Our bottleneck simply produces less work
          than can be processed by all subsequent steps.
          In the world of manufacturing there's usually some technicality that defines the rate limiting step, but
          in Software Development we often have more control over this - we can simply add or remove people from some
          steps.</p>
        <p>Things get even simpler because in software projects there're only 2 primary steps - Development and Testing.
          Others are not as crucial (in terms of the buffers):</p>
        <ul>
          <li>BAs, UX - could sometimes become bottlenecks. But usually they can whip up a feature or two
            by request relatively quickly. So oftentimes we can omit thinking about their performance.</li>
          <li>Release & Configuration management - these days is mostly a solved problem and can be automated.</li>
        </ul>
        <p>So what's going to happen if we have <i>redundancy in Testing</i>? Its buffer will almost always be empty!
          So the problem of big vs. small task is not a problem - once a task is implemented it can be accepted into
          Testing immediately. This has additional benefits:</p>
        <ul>
          <li>Testers have free time and in addition to functional testing can think about UX, security, performance, etc.</li>
          <li>Developers receive feedback very quickly while they still remember what they developed</li>
        </ul>
        <p>Note that when there're only 2 steps in a system both JIT and ToC become equal things.</p>

        <h2>Summary</h2>
        <p>We learned that <del>Kanban</del> JIT has issues when the amount of work varies greatly from time to time.
          And this is probably one of the reasons why Lean sees standardization as a an important part of
          Continuous Improvement. Ideally we should have smaller tasks and push them to testing frequently which
          would certainly alleviate the problem.
        </p>
        <p>
          But this is not always possible or optimal. And since in Software Development we're not bound by the constraints
          of the physical world, we can move around resources and <i>decide</i> where the bottleneck resides.
          This makes it relatively easy to fight with variability - by employing principles from Theory of
          Constraints we can make the 1st step in the system the slowest in order to guarantee empty buffers and
          hence low Inventory Costs.</p>

        <h2>Related articles</h2>
        <ul>
          <li><a href="./lean-jit-kanban.html">Lean vs. JIT vs. Kanban</a></li>
        </ul>

        <h2>Popup Notes:</h2>
        <ul>
          <li>
            <div id="one-step-is-always-slower">
              <h4>Some Theory of Constraints wisdom</h4>
              <p>Actually one of the steps can and MUST be idle from time to time. This simply shows that one of
                them works faster than the other. No matter how we try this is going happen unless you want to abandon
                the ideas of JIT and ToC and use a Push system (which is way less effective).</p>
            </div>
          </li>
          <li>
            <div id="constraints-forever">
              <h4>Constraints always exist</h4>
              <p>We <i>can't</i> really eliminate a constraint. It's possible to optimize the constraining step
                but this will simply turn another step into a constraint. Such back-and-forth creates chaos which we don't want.</p>
            </div>
          </li>
        </ul>
    </div>
  </div>
</div>
</div>
<div class="quote-modal" id="quote-modal">
  <div><a href="#" id="quote-modal-close" title="Close" class="close">X</a>

    <div id="quote-modal-content">
      <h4>Title</h4>
    </div>
  </div>
</div>
