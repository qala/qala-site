---
title: "Building Test Pyramid to optimize automated testing"
layout: "blogEntry"
isPage: true
---
<div class="blog-container">
  <div>
    <div class="blog-main">
      <div class="blog-post">

        <h1 class="blog-post-title">Building Test Pyramid to optimize automated testing</h1>

        <p class="blog-post-meta">September 28, 2015 by Stanislav Bashkyrtsev</p>
        <p>Tired of fragile tests that break without a reason? Full test run takes hours and you feel sick of endless
          test optimization? Waking up in the middle of the night screaming SELENIUM PLEASE NO?
          These are common symptoms of a wide-spread disease called Hypopyramidism. Traditional treatments are usually
          symptomatic: fine-tuning timeouts, running tests in parallel, taking anti-depressants and so on. But a real
          cure exists. And you've probably heard of it before. Its name is Test Pyramid and in this article we'll
          use it to make our tests fast and furious.</p>
        <hr>

        <h2>So what is a Test Pyramid?</h2>
        <p>Test Pyramid describes a simple idea that the more complex the tests are - the smaller number of them you
          should write. Traditionally every pyramid is drawn as a triangle hehe:</p>
        <img src="/img/test-pyramid.png"
             style="margin-left: auto; margin-right: auto; display: block;max-height:300px">
        <p>The layers correspond to Unit, Component and System tests. According to
          <a href="holes-in-test-terminology.html">our terminology</a>:</p>
        <ul>
          <li>Unit Tests concentrate on functions and classes. These are very simple, fast and stable.</li>
          <li>Component Tests check whether those classes, when joined together, do what was intended. They don't
            require full app being deployed. Often people call them Integration Tests. These tests are not as fast or
            simple as unit tests, but still are very stable.
          </li>
          <li>System Tests are run against fully deployed app. These are very complicated and slow tests. And
            they depend on both stability of the testing tools, the app under test and infrastructure. Every part can
            break and we may get a false-negative test run.
          </li>
        </ul>
        <p>While a lot of people know this, in practice we have a totally different picture - a lot of unnecessary
          testing is done at System Level. I've seen a project where they ran their tests in <i>10 threads</i> and the full
          run took <i>3 hours</i>! </p>
        <p>In a different project they had a well-known problem that they couldn't run
          all the tests without a failure. A glitch-like failure, not a real bug. They handled it in a very
          peculiar way - a threshold was introduced: if the number of failures were below the bar,
          they treated it as green. Very deterministic testing!</p>
        <p>I myself had a chance to write tests in this way for some time. Ended up with the full run taking 5 hours.
          And that was after the optimizations - we were running a part of them in headless browser. Was it worth it?
          Let's say that it was a nice experience of how we shouldn't do.</p>
        <p>But hey - let's break this vicious tendency. Keep reading and you'll see how we can change it for better.</p>



        <h2>Example of Test Pyramid (Groovy + AngularJS)</h2>

        <p>Are you ready to build a test pyramid? Note though that it depends a lot on the technologies
          we use in the app. The pyramid for your app will probably look very different! Below we'll consider
          a sample app which is called... Test Pyramid! It's written with AngularJS, Spring MVC and Hibernate which
          dictates a lot how the tests look. You can find both production and test sources
          <a href="https://bitbucket.org/qala/test-pyramid">at the BitBucket repo</a>. Here we only will show a small
          set of those to illustrate what logic needs to be tested at what level.</p>

        <p>Couple of words on the application we're going to test now: you can create pyramids with Name,
          N of System/Component/Unit tests and those pyramids are saved into DB. There is basic validation both
          on UI and on Back End. You can click on the pyramid in the list and it will be drawn for you.</p>


        <h3>Server Side Unit Tests</h3>

        <p>Here's a good candidate for Unit Testing, the only logic present is validation rules:</p>
        <pre><code class="language-java">class Pyramid {
    Long id
    @NotNullSized(min = 1, max = 100)
    String name
    @Min(0L)
    int nOfUnitTests
    @Min(0L)
    int nOfComponentTests
    @Min(0L)
    int nOfSystemTests
}</code></pre>
        <p>The code is simple and we don't need to initialize a lot to test it. We use
          <a href="http://spockframework.org/">Spock</a> here since validation
          is easier to test with data-driven tests at which Spock is very good. And we use
            <a id="datagen-link" href="https://github.com/qala-io/datagen/">Datagen</a> to randomize test data:</p>
        <pre><code class="language-java">class PyramidTest extends Specification {
  @Unroll
  def 'validation for name must pass if #valueDescription specified (#name)'() {
    given:
      Pyramid pyramid = Pyramid.random([name: name])
      Set violations = validator().validate(pyramid)
    expect:
      pyramid && 0 == violations.size()
    where:
      name                            | valueDescription
      randomAlphanumeric(1)           | 'min boundary value'
      from(2).to(99).alphanumeric()   | 'typical happy path value'
      randomAlphanumeric(100)         | 'max boundary value'
      from(1).to(99).numeric()        | 'numbers only'
      from(1).to(99).specialSymbols() | 'special symbols'
}</code></pre>
        <p>It simply creates an object of class Pyramid, sets the name to whatever value is needed for the test and
          checks that validation logic returns an empty set of violations. In this example we covered only positive
          cases for name validation, see the project sources for other tests.
          Every line of the <code>where</code> block will resolve in its own test case:</p>
        <pre><code>validation for name must pass if min boundary value specified (c)
validation for name must pass if typical happy path value specified (z8mMVw0nbUKKxCjsCgnJoBqG2JyF9Jcax8Gr69Y0ZElds1YKy)
validation for name must pass if max boundary value specified (lZ1M1yBMa0STfYbj6KntkI9mGlxlxhff2...)
validation for name must pass if numbers only specified (941525639352684027677613208)
validation for name must pass if special symbols specified ((§*<!``(}|~:]\%:^§)>:$'"'`[)</code></pre>
        <p><b>Important Notes:</b></p>
        <ul>
          <li>Isn't that very different from what we often do in practice? Usually this is a System Test which takes
            1000x more time to run.</li>
          <li>We invoked a Validator (this is a separate lib
            that implements <a href="http://beanvalidation.org/1.1/spec/">Bean Validation spec</a>) in the test itself,
            it doesn't mean that our app will invoke it and will treat the results correctly! Keep reading.</li>
        </ul>


        <h3>Server Side Database Tests</h3>

        <p>Traditionally a lot of projects have a separate layer for that. Terms differ (DAO - Data Access Object,
          DAL - Data Access Layer, Repository), but the general idea is the same - you've got separate set of classes
          responsible for work with DB:</p>
        <pre><code class="language-java">class PyramidDao {
  Pyramid save(Pyramid pyramid) {
    session.save(pyramid);
    return pyramid
  }

  List list() {
    return session.createQuery('from Pyramid').list()
  }
}</code></pre>
        <p>This is handy since we can have separate set of tests that cover DB logic. In this case there is no
          data-driven tests, therefore we'll use Groovy JUnit to keep tests compact:</p>
        <pre><code class="language-java">@ContextConfiguration(locations = 'classpath:/io/qala/pyramid/domain/app-context-service.groovy')
@Transactional(transactionManager = 'transactionManager')
@Rollback
@RunWith(SpringJUnit4ClassRunner)
class PyramidDaoTest {
  @Test
  void 'must be possible to retrieve the Pyramid from DB after it was saved'() {
    Pyramid pyramid = dao.save(Pyramid.random())
    dao.flush().clearCache()
    assertReflectionEquals(pyramid, dao.list()[0])
  }

  @Test
  void 'must treat SQL as string to eliminate SQL Injections'() {
    Pyramid pyramid = dao.save(Pyramid.random([name: '\'" drop table']))
    dao.flush().clearCache()
    assertReflectionEquals(pyramid, dao.list()[0])
  }
}</code></pre>
        <p><b>Important Notes:</b></p>
        <ul>
          <li>We test SQL Injection at this level. Which is often done at System Level and is 100x times
            slower</li>
          <li>These are Component Tests since they require a large part of the app to be initialized
            (note <code>@ContextConfiguration</code>)</li>
          <li>These are Component Tests since they require In-Memory DB to be initialized (like
            <a href="http://hsqldb.org/">HSQLDB</a>). It
            imitates the real DB. If needed these test can be run against the real DB as well.</li>
        </ul>
        <p>Even though these are Component Tests, we consider and treat them separately since they are very special.</p>


        <h3>Server Side Component Tests</h3>

        <p>Every application has its entry points and in our case these are Spring MVC Controllers/REST Services.
          Usually those are accessed when HTTP request hits the App Server which in turn passes it to the entry points.
          But imagine if we could hit those entry points without HTTP - by directly invoking objects and their methods.
          And that's what we're going to do now. Here are our entry points that handle <code>/</code>,
          <code>/pyramid</code> and <code>/pyramid/list</code> URLs respectively:</p>
        <pre><code class="language-java">@RequestMapping(value = '/', method = RequestMethod.GET)
ModelAndView index() {
  return new ModelAndView('index', [savedPyramids: new JsonBuilder(pyramidService.pyramids()).toString()])
}

@RequestMapping(value = '/pyramid', method = RequestMethod.POST)
@ResponseBody
Pyramid save(@Valid @RequestBody Pyramid pyramid) {
  pyramidService.save(pyramid)
  return pyramid
}

@RequestMapping(value = '/pyramid/list', method = RequestMethod.GET)
@ResponseBody
List> pyramids() { return pyramidService.list() }</code></pre>
        <p>Some of them simply generate an HTML page, others are REST services that operate with JSON representation
          of Pyramid class. Different web frameworks (Spring MVC, RestEasy, Jersey, etc.) provide different frameworks
          to test them. In our case it's <code>MockMvc</code>:</p>
        <pre><code class="language-java">MvcResult result = mockMvc.perform(post('/pyramid')
  .content(new JsonBuilder(pyramid).toPrettyString())
  .contentType(MediaType.APPLICATION_JSON)).andReturn()</code></pre>
        <p>If we put this code directly into test it'll be overloaded with technical stuff and we'll hardy follow the
          code, therefore it makes sense to encapsulate this logic into separate layer of your tests (in our case this
          is a class <code>Pyramids</code>). After that the tests look clean and readable for most people:</p>
        <pre><code class="language-java">@RunWith(SpringJUnit4ClassRunner)
@WebAppConfiguration
@ContextConfiguration(locations = [
  'classpath:/io/qala/pyramid/domain/app-context-service.groovy',
  'classpath:/spring-mvc-servlet.groovy',
  'classpath:/app-context-component-tests.groovy'])
class PyramidComponentTest {
  @Autowired Pyramids pyramids

  @Test
  void 'service must save a valid pyramid'() {
    Pyramid pyramid = pyramids.create()
    pyramids.assertPyramidExists(pyramid)
  }

  @Test(expected = MethodArgumentNotValidException)
  void 'service must return errors if validation fails'() {
    pyramids.create(Pyramid.random([name: '']))
  }
}</code> </pre>
        <p>Interesting fact - on one of my projects there were a number of System Level tests that were running
          against REST services. We made it possible to run them both against services and using direct object
          invocation. And the timing for 800 tests run dropped from 9 mins to 2.</p>
        <p><b>Important Notes:</b></p>
        <ul>
          <li>These tests initialize almost the whole app - starting from the entry points and ending up with DAO and
            In-Memory DB. But they don't require the Application Server and can be run along the Unit Tests!</li>
          <li>These tests allow you to get rid of mocking in unit tests. The problem with mocks - they interact with the
            internal logic of your classes and therefore will change every time that logic changes. Also, we often find
            ourselves in situation when we mock a lot and therefore we mostly test how we initialize our mocks rather
            than how our business logic works (remember those weird tests for Service Layer? BTW, where are the tests
            for the Service Layer? ;)). So mocks are evil, but sometimes are necessary. Component Tests will free us
            from unnecessary mocking.</li>
          <li>These tests check whether validation is invoked by production code. Note, that there are no massive checks
            of validation rules - that's left in the unit tests. Instead we violate only one rule (empty pyramid name)
            and check if that resulted in an error. That's it - we know the the rules are respected and the validation
            is invoked!</li>
          <li>Because all the communication with Controllers/REST services in this case is via JSON we also check
            Serialization/Deserialization.</li>
        </ul>


        <h3>Server Side System Tests</h3>

        <p>And we conclude our Server Side series with System Tests which check the REST Services:</p>
        <pre><code class="language-java">@Test
void 'add pyramid should allow to successfully retrieve the pyramid'() {
  def json = pyramid()
  rest.post(path: '/pyramid', body: json.toString())

  def expected = json.content
  def pyramid = rest.get(path: '/pyramid/list').data.find { it.name == expected.name }
  assert pyramid
  assert pyramid.nOfUnitTests == expected.nOfUnitTests
  assert pyramid.nOfComponentTests == expected.nOfComponentTests
  assert pyramid.nOfSystemTests == expected.nOfSystemTests
}
@Test
void 'add invalid pyramid should result in validation errors'() {
  def json = pyramid([name: ''])
  Throwable error = shouldFail(HttpResponseException) {
    rest.post(path: '/pyramid', body: json.toString())
  }
  assert error.message.contains('Bad Request')
}</code> </pre>
        <p><b>Important Notes:</b></p>
        <ul>
          <li>There are only couple of tests on that level - we need to do only the basic checks for every endpoint</li>
          <li>These kind of tests don't check the business logic - it was already checked. Here we test only the fact
            that things like <code>web.xml</code> or App Server descriptor are written correctly.</li>
        </ul>


        <h3>UI Unit Tests</h3>

        <p>There is much more logic at UI. Let's have a look at the logic that calculates the percentage of every test
          level in the created pyramid:</p>
        <pre><code class="language-javascript">function updateProportions() {
   var sum = self.tests.reduce(function (prevValue, it) {
     return prevValue + (+it.count || 0);
   }, 0);
   self.tests.forEach(function (it) {
     it.proportion = sum ? it.count / sum : 0;
     if (!it.count || isNaN(it.count)) {
       it.label = '';
     } else {
       it.label = +(it.proportion * 100).toFixed(1) + '%';
     }
   });
   return [self.unitTests.proportion, self.componentTests.proportion, self.systemTests.proportion];
 }</code> </pre>
        <p>That looks tough - I know what's going on here only because I wrote it. When field <code>self.tests</code>
          is updated it's used by UI to be shown as labels near inputs. If all input fields are empty, then
          empty labels will be shown. If there are digits, then the sum will be calculated and the percentage of every
          test type will be calculated.</p>
        <p>Now, let's see couple of tests that cover this logic. They are written in
          <a href="http://jasmine.github.io/2.0/introduction.html">Jasmine</a> +
          <a href="http://karma-runner.github.io/0.13/index.html">Karma</a>:</p>
        <pre><code class="language-javascript">it('test percentage must be empty if sum is more than 0 and one of test counts is non-numeric', function () {
    sut.currentPyramid.unitTests.count = moreThanZero();
    sut.currentPyramid.componentTests.count = alphabetic();
    sut.updatePercentage();

    expect(sut.currentPyramid.unitTests.label).toBe('100%');
    expect(sut.currentPyramid.componentTests.label).toBe('');
 });
 it('must set empty to other test percentages if only count for system tests was filled', function () {
   sut.currentPyramid.systemTests.count = moreThanZero();
   sut.updatePercentage();
   expect(sut.currentPyramid.componentTests.label).toBe('');
   expect(sut.currentPyramid.unitTests.label).toBe('');
 });</code></pre>
        <p>These tests check that the labels are updated to 100% or to empty values in cases the digits are
          entered or not.</p>
        <p><b>Important Notes:</b></p>
        <ul>
          <li>Since this logic is algorithmic, it fits perfectly for unit testing. There may be so many of these cases
            - and therefore they will require so many tests. If we write them at System Level we'll quickly end up with
            runs that take hours.</li>
          <li>While we checked that our calculations are correct, how do we know that the UI really shows these values?
            This question will be answered next.</li>
        </ul>


        <h3>UI Component Tests</h3>

        <p>Prepare yourself since this will be the hardest part of the tests! With frameworks like AngularJS
          we have a lot of logic both in JS and HTML. While JS logic can be covered
          by unit tests, to test how this JS is triggered by HTML and how it impacts HTML we'll need to check fully
          functioning UI. So here is a piece of HTML which we'll cover:</p>
        <pre><code class="language-markup"><script type="prism-html-markup"> <input data-ng-pattern="'[0-9]+'" maxlength="8"
        data-ng-model="testType.count" data-ng-change="pyramid.updatePercentage()"></script></code> </pre>
        <p>Plenty of logic is present here:</p>
        <ul>
          <li>Only 8 symbols can be entered into the field</li>
          <li>Characters need to satisfy pattern <code>[0-9]+</code></li>
          <li>Input value is bound to fields of AngularJS Controller (<code>data-ng-model="testType.count"</code>)</li>
          <li>When value of the input is changed, a function needs to be triggered:
            <code>data-ng-change="pyramid.updatePercentage()"</code></li>
        </ul>
        <p>Phew! Too much is going in these couple of lines, isn't it? The tools to cover HTML pages are well
          known - Selenium + <a href="https://angular.github.io/protractor/#/">Protractor</a>
          (the latter is a wrapper around <a href="https://code.google.com/p/selenium/wiki/WebDriverJs">WebdriverJS</a>
          that
          targets AngularJS apps specifically). But if we test against a fully deployed app we're back to hours of test
          runs. So how do we speed them up? Here are the possibilities for improvements:</p>
        <ul>
          <li>Both tests, Selenium and the app need to be located on the same machine to eliminate network traffic
            between these components.</li>
          <li>Server Side needs to be fully mocked to get rid of application server and database. Besides pure
            page load optimization it will give the control over what we initialize in UI and will provide
            shortcuts to access deep parts of the UI (e.g. the page that we could see only when we're logged in).</li>
        </ul>
        <p>So first of all before the tests start we instantiate a NodeJS server that serves static resources and
          handles AJAX queries:</p>
        <pre><code class="language-javascript">app.use('/favicon.ico', express.static(path.join(self.webappDir, 'favicon.ico')));
app.use('/vendor', express.static(path.join(self.webappDir, 'vendor')));
app.use('/js', express.static(path.join(self.webappDir, 'js')));
app.use('/css', express.static(path.join(self.webappDir, 'css')));

app.get('/', function (req, res) {
  res.render('index.html.vm', {savedPyramids: JSON.stringify(self.pyramids)});
});
app.post('/pyramid', function (req, res) {
  var pyramid = Pyramid.fromJson(req.body);
  self.addPyramid(pyramid);
  res.status(200).json(pyramid);
});</code> </pre>
       <p>And then we write the tests themselves that type into the inputs and observe the results:</p>
       <pre><code class="language-javascript">it('unit tests field must be empty by default', function () {
  homePage.clickCreate();
  expect(homePage.getNumberOfTests('unitTests')).toBe('');
});
it('unit tests label must be updated as we type', function () {
  homePage.clickCreate();
  homePage.fillNumberOfTests('unitTests', 10);
  expect(homePage.getLabel('unitTests')).toBe('100%');
});</code></pre>
        <p>We omit Page Objects here to keep article shorter, you can find that code at BitBucket.</p>
        <p>Many people would argue about the necessity of Server Side mock - won't we implement the back end logic
          twice? To be honest - I haven't tried this in real projects yet (though soon I will try it out), so take
          my reasoning with a grain of salt:</p>
        <ul>
          <li>The mock will be much easier than a real back end. It's like mocking in the unit tests - you have to
            write some logic there as well, don't you?</li>
          <li>UI Component Tests won't be failing because of the bugs from Server Side. And we really don't want that
            to happen since additional re-runs of these tests will take a lot of time.</li>
          <li>The real Server Side may not provide enough of flexibility to test variety of situations. These situations
            may not be possible from server side at the moment, but UI may still need to be tested with those.</li>
          <li>It will hardly be possible to install CI Agents and Selenium at the machines where the real app is
            deployed. Ergo, we cannot eliminate network communication. But NodeJS mock can be run along with the
            tests at the build machine (yeah, that build machine still would need Selenium and browsers).</li>
          <li>Again - with real Server Side you may need some data preparation and additional steps (to sign up, sign
            in, create some items) before you get to a place where you can test some of the UI functions. With mocks
            it's much easier.</li>
          <li>UI Development in general should be faster with those. Deployment is easier and much quicker. And if
            there is a nasty bug on the Back End, it doesn't block UI folks!</li>
        </ul>
        <p>To sum up - since UI tests are so complicated and so costly it makes sense to work on their optimization. If
          you spend time implementing a mock you'll free yourself from constant problems with false-negatives which
          in turn takes time because you have to read reports, reproduce issues and re-run these tests.</p>
        <p><b>Important Notes:</b></p>
        <ul>
          <li>These tests are concentrated on how UI behaves. They don't check calculations or server side
            communication.</li>
          <li>A run of these tests should be the longest among all of the tests in apps where a lot of logic is on UI.
            This is because there are many of the tests, but in the same time they need to be run in browses which
            is very slow. </li>
          <li>To keep tests simple and predictable you'll need to make HTML pages testable. For that you'll have to
            include IDs, names, custom attributes so that it's easy to bind to the elements. That'll pay off quickly.
          </li>
        </ul>


        <h3>UI System Tests</h3>

        <p>Finally! This is the last part of the tests we'll consider! And it will be short, here are the tests:</p>
        <pre><code class="language-javascript">it('adds newly added item to the list of pyramids w/o page reload', function () {
  var pyramid = homePage.createPyramid();
  homePage.assertContainsPyramid(pyramid);
});
/** This is done by server side when page is generated. */
it('shows item to the list of pyramids after refresh', function () {
  var pyramid = homePage.createPyramid();
  homePage.open();
  homePage.assertContainsPyramid(pyramid);
});
/** This is done by server side when page is generated. */
it('escapes HTML-relevant symbols in name after refresh', function() {
  var pyramid = homePage.createPyramid(new Pyramid({name: '\'">'}));
  homePage.open();
  homePage.assertContainsPyramid(pyramid);
});</code> </pre>
        <p><b>Important Notes:</b></p>
        <ul>
          <li>These tests are mostly concentrated on how UI collaborates with the Back End. They don't check the
            business logic (and therefore there are few of them), they don't check the UI (and therefore there are
            few of them).</li>
          <li>Since we've got our tests that check how UI works on lower level, we don't have to run UI System Tests
            in real browsers - we can run them in headless mode (HtmlUnit, PhantomJS).</li>
          <li>The same Page Objects and other test layers can be re-used between Component Tests and System Tests.
            Ergo, we don't duplicate - all the scaffolding was prepared on previous stages.</li>
        </ul>



        <h2>So let's sum up?</h2>

        <p>Tests must be fast and reliable. If they take hours and fail from time to time - people stop trusting them,
          people stop counting on them. You've done a good job if:</p>
        <ul>
          <li>Your tests give the feedback very fast before the author of a change switched to another task</li>
          <li>You don't re-run the tests after a failure to find out if it was a glitch</li>
          <li>Your tests don't use mocking libraries or their usage is very limited</li>
          <li>The number of System Tests is tiny comparing to others</li>
        </ul>

        <p>Remember that your pyramid may look totally different from what you've seen in this article. If you
          don't have a lot of logic on UI you may not be able to write UI Component Tests since the page
          generation depends a lot on server side. If you have different technologies like Spring JDBC
          instead of a full-blown ORM then your DB Tests may be very different from the aforementioned. And the list
          can go on, so prepare to be creative, your pyramids will be unique!</p>

        <h2>Interesting literature on the topic</h2>
        <ul>
          <li><a href="http://fabiopereira.me/blog/2012/03/18/introducing-depth-of-test-dot/" target="_blank">Depth of Test</a> by ThoughtWorks</li>
          <li><a href="http://googletesting.blogspot.ru/2015/04/just-say-no-to-more-end-to-end-tests.html" target="_blank">
            Just Say No to More End-to-End Tests</a> by Google</li>
          <li><a href="http://jamescrisp.org/2011/05/30/automated-testing-and-the-test-pyramid/" target="_blank">
            Automated Testing and the Test Pyramid</a> - nice article about Cost/Benefits of automated testing and
            Gherkin/Behave testing in particular.</li>
           <li><a href="http://www.rbcs-us.com/documents/Why-Most-Unit-Testing-is-Waste.pdf" target="_blank">
               Why Most Unit Testing is Waste</a> - an interesting viewpoint on the pyramid
               - author suggest to get rid of the unit tests as they take more than they give.</li>
        </ul>

        <h2>Next Steps</h2>
        <ul>
            <li><a href="anaemic-architecture-enemy-of-testing.html">Anaemic Architecture - enemy of testing</a> is
                about how your project's architecture should look in order to support Test Pyramid.</li>
            <li><a href="randomized-testing.html">Randomized Testing - What, Why, When?</a> is about how to further
                simplify and speed up your tests as well improve the coverage.</li>
            <li><a href="evolution-of-automation-test-engineer.html">Evolution of Automation Test Engineer</a> on
                how Selenium tests can evolve from ugly looking ones to the business-oriented cases with OOP applied.</li>
        </ul>
      </div>
      <!-- /.blog-post -->
    </div>
    <!-- /.blog-main -->
  </div>
  <!-- /.row -->
</div>

<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "BlogPosting",
      "headline": "Building Test Pyramid to optimize automated testing | Qala",
      "alternativeHeadline": "Test Pyramid as a cure for slow and fragile tests | Qala",
      "datePublished": "2015-09-28T00:00:00+00:00",
      "description": "Tired of fragile tests that break without a reason? Full test run takes hours and you feel sick of endless test optimization? Waking up in the middle of the night screaming SELENIUM PLEASE NO? These are common symptoms of a wide-spread disease called Hypopyramidism. Traditional treatments are usually symptomatic: fine-tuning timeouts, running tests in parallel, taking anti-depressants and so on. But a real cure exists. And you've probably heard of it before. Its name is Test Pyramid and in this article we'll use it to make our tests fast and furious."
    }
</script>