---
title: "Randomized Testing - What, Why, When?"
layout: "blogEntry"
isPage: true
---
<div class="blog-container">
  <div>
    <div class="blog-main">
      <div class="blog-post">

        <h1 class="blog-post-title">Randomized Testing - What, Why, When?</h1>

        <p class="blog-post-meta">November 7, 2015 by Stanislav Bashkyrtsev | Last Edited: 28 May, 2016</p>
        <p>
          Still hardcoding concrete values in your tests? That's a shame since better techniques exist that provide
          better coverage and test isolation.
          Let's talk about how we can improve testing by adding randomization. We'll cover some of the
          techniques like Property/Model Based Testing, Test Oracles, etc.
          The purpose is not to dive deeply into the details of each different kind,
          but to give an overview and show possible applications.
        </p>
        <p>By the way, if you know Russian and don't like reading
          <a href="https://www.youtube.com/watch?v=cA_A-BSqT50">here is my talk</a> at Heisenbug 2016 that covers
          randomized testing in more details and with more examples.</p>


        <h2>Why hardcoded test data is bad?</h2>

        <p>Suppose that we test a validation. E.g. First Name field can accept alpha numeric values from 2 to 20 symbols
          long. We've done the equivalence partitioning and for happy path we chose a value from 3 to 19 - e.g. Barney.
          But this has drawbacks:</p>
        <ul>
        <li><b>No clear winner</b>. Why Barney? Why not Michael, Ohno or Fedor?</li>
        <li><b>Prone to Pesticides Effect</b>. While the codebase increases in size, the odds of
          new bugs creeping into First Name validation increases. But because we use the same "pesticide" (test) and
          don't change it, the chances that we don't find those defects go up as well. We can say that bugs
          become resistant to one particular pesticide and it doesn't prevent them from multiplying.
          If only we were using many different pesticides..</li>
        <li><b>Poor test isolation</b>. If we write System Tests that run against the same environment
          again and again, how do we go about unique fields? Emails often must be unique, what would happen
          if we run same test twice and it'll try to create 2 users with the same email? The 2nd run will fail.
          Even if we decide to clean data some how, we're
          still prone to race conditions - what if multiple people run the same test from their local machines?</li>
        </ul>


        <h2>Fixing test data with randomization</h2>

        <p>Randomized testing addresses all the concerns gracefully - instead of taking a particular value we'll
          generate it randomly every time. Let's compare the approaches:</p>
        <table class="table table-condensed" style="text-align: center">
          <tr>
            <th style="text-align: center">Partition</th> <th style="text-align: center">Example-based</th> <th style="text-align: center">Randomized</th>
          </tr>
          <tr>
            <td>Happy Path</td> <td>Barney</td> <td><code class="language-java">alphanumeric(3, 19)</code></td>
          </tr>
          <tr>
            <td>Positive Min Boundary</td> <td>Mi</td> <td><code class="language-java">alphanumeric(2)</code></td>
          </tr>
          <tr>
            <td>Positive Max Boundary</td> <td>Blah Blah Blah Blah1</td> <td><code class="language-java">alphanumeric(20)</code></td>
          </tr>
          <tr>
            <td>Negative Min Boundary</td> <td>a</td> <td><code class="language-java">alphanumeric(1)</code></td>
          </tr>
          <tr>
            <td>Negative Max Boundary</td> <td>Blah Blah Blah Blah12</td> <td><code class="language-java">alphanumeric(21)</code></td>
          </tr>
          <tr>
            <td>Numbers only</td> <td>12345</td> <td><code class="language-java">numeric(2, 20)</code></td>
          </tr>
          <tr>
            <td>With spaces</td> <td>aB 0A</td> <td><code class="language-java">between(2, 20).with(spaces()).alphanumeric()</code></td>
          </tr>
          <tr>
            <td>...</td><td>...</td><td>...</td>
          </tr>
        </table>

        <p>You can use libraries like <a id="datagen-link" href="https://github.com/qala-io/datagen/">Datagen</a> that
          already implementthis kind of randomization in a nice way.</p>
        <p>Notice, that we still leverage boundary values and equivalence partitioning. This is because bugs are
          usually concentrated on the boundaries. And we have either a choice of running the same test many times
          to be almost sure that the boundary value was used or we can optimize our tests by using boundaries
          explicitly. And the randomization happens <i>within</i> the equivalence class.</p>

        <h2>Does it find more bugs in reality?</h2>

        <p>In simple cases like validation it's not finding a lot of bugs comparing to traditional approaches.
          Otherwise the whole idea of equivalence classes would be invalid. After randomizing validation tests
          for couple of years
          the only case I remember when it helped to find more bugs is when we were testing date conversion that
          didn't work properly for some not-commonly-used dates.
          But this fact shouldn't stop you from using randomization - don't forget about other goodies like test
          isolation.</p>
        <p>Validation though is just one of the applications. Randomized testing can be even more
          beneficial when testing business logic, algorithms and concurrency.</p>


        <h2>Testing algorithms with random data</h2>

        <p>With validation things are easy - we know the result when we're crafting the input. But with algorithms it's
          different - if the input is random, then the output is random. So how do we check the output if we
          don't know it beforehand? Implementing the same algorithm in the tests is absurd!</p>


        <h3>Property Based Testing</h3>

        <p>Instead of checking the actual result we can check its properties. So if we test summation we can check
          that <code>a + b == b + a</code> or if <code>a + 0 == a</code> where <code>a, b</code>
          are randomly generated values. Or if you test sorting, you could generate random list of objects,
          sort it and then go over each element and check if it's >= next element.
        <p>Often it's combined with the repeating of the same tests many times to increase the probability of finding
          defects.
          There are specialized frameworks that can do this for you - they usually have names ending with QuickCheck
          which comes from the pioneering Haskell's
          <a href="http://www.cse.chalmers.se/~rjmh/QuickCheck/manual.html">QuickCheck</a>. E.g. for Java there is
          <a href="http://pholser.github.io/junit-quickcheck">JUnit QuickCheck</a>. Additionally
          those frameworks often provide so called shrinkage - if test failed with value 1000 but passed with 500
          the framework will start picking values in between to find the boundaries where failures start and
          where they end.</p>
        <p>While this is a very impressive technique it's also extremely complicated. When I tried it I could spend
          couple of days thinking on how to fully cover my algorithm (and I haven't finished). It requires
          practice and mathematical thinking, so be prepared to meet difficulties. On the bright side - you don't
          have to cover your code at 100% level with this technique, you can still combine it with example-based testing
          to fill the holes.</p>
        <p>You can read more about Property
          Based Testing in
          <a target="_blank" href="http://fsharpforfunandprofit.com/posts/property-based-testing/">this nice article</a>.
        </p>


        <h3>Using Test Oracles</h3>

        <p>Another option and probably the most cost effective one is using Test Oracles. If there is an easier
          (but not as optimal or secure) way of doing the same thing, you can use a simpler
          implementation in the tests and cross-check the results of both algorithms. E.g. you can use a default
          sorting of your Programming SDK as a reference implementation while checking your super-fast O(1) and
          super-complicated sorting algorithm.</p>


        <h3>Reverse operations to get the original value</h3>

        <p>This is particularly useful if you transform objects from one another (DTO->Entity, Entity->DTO). You
          fill the ObjectA with values randomly, then convert it into ObjectB, then back into ObjectA'. If ObjectA
          equals ObjectA' then both transformations work correctly. Very efficient testing especially if you
          employ reflection-based comparison (e.g. <code>reflectionEquals()</code> from
          <a target="_blank" href="http://unitils.org/summary.html">Unitils</a>).</p>
        <p>But it's not limited to DTO-Entity transformations. A lot of operations may have reverse sisters:
          string tokenizer + concatenation, string occurrence replacing + replacing it back,
          element insertion + element removal, etc. The principle is the same - after the operation you apply a
          reverse function and check that the result is the same as the original input.</p>


        <h2>Model Based Testing to test states and transitions</h2>

        <p>I'm still waiting for a possibility to try it out, but I can't resist mentioning Model Based Testing after
          watching <a href="https://www.youtube.com/watch?v=zi0rHwfiX1Q">this inspiring presentation</a>. Instead of
          randomly generating <i>data</i> as we did previously this approach generates random <i>behaviour</i>.
          The idea is that
          we build a model of our app: ActionA can be done after ActionB but only if ActionC happened. Then we ask
          our framework to invoke these operations in random order (it must follow the rules though). After that we
          can check that the expected side-effect (final result) is what we described in the model.</p>
        <p>This can be useful when testing complicated flows transitioning from state to state. But what's marvelous
          is that you can test it in concurrent environment as well. With that you don't only check that the flow
          is correct, but also that it works under high load with many concurrent users. After all if we bought a
          ticket 10 times then in the end we should have 10 unique tickets even if we did that in multiple threads.</p>
        <p>Some of the frameworks can also provide the shrinkage - they will try to find the minimal sequence of
          steps that result in bug. And that is so important because after doing 1000 steps in 10 threads it'll
          be hard to reproduce the problem because of so much noise.</p>


        <h2>Decreasing number of combinations to test</h2>

        <p>Sometimes there are object graphs where each object could be in different states. Returning to our
          example: 20 account types, 30 legal entity types with 5 account roles each. If the business rules for
          different types or roles may differ we should test that. But there are so many combinations - it may take
          too much time to run the tests. Especially if there are many features that we want to test for these combinations.
        </p>
        <p>But if we build a model according to which we can generate these object graphs correctly, we can partition
          those combinations and check only a single one from a given equivalence class. This can reduce the number
          of tests from 1K to just 3. In case some particular combination is buggy - at some point we'll know that
          because these combinations differ from run to run.</p>
        <p>Even if the number of combinations is manageable, at some point when the functionality is stabilized
          we can reduce the number of cases by checking only small subset of them each time. By doing this we can
          speed up our tests.</p>


        <h2>Randomizing Environment Settings</h2>

        <p>We can also randomize user locales, OS that's used to run the tests, SDK versions, etc. Of course only
          those environments that are expected to be compatible with our app should be used.</p>


        <h2>But I want tests to be reproducible and stable!</h2>

        <p>Well, that's true - the test needs be designed in a way that it won't fail (the test itself) in different
          environments. I.e. we don't want the test to fail just because
          someone's machine is too fast or too slow. But we still want
          to find real bugs. So if production code (not test) fails because we changed the environment while we
          expect it to work on that env, then we found something. If a test fails for a reason, then the test
          did its job right. After all - this is the reason we write tests.</p>
        <p>But how do we reproduce the test if it relies on randomness? For that we need one of two:
          a) logging (values, environment) b) ability to set seed for random generator. Ideally we need both.
          Haven't worked
          with manually set seeds myself, but I can see how helpful it may be. If a failure happens we can reuse the
          same seed and all the "random" values will repeat.</p>


        <h2>Randomizing manual tests</h2>

        <p>Randomization is applicable in manual tests as well though it won't be as chaotic:</p>
        <ul>
          <li><b>Randomizing data</b>. You can achieve that by saying "Enter alpha numeric value from 3 to 29 symbols
            long"
            instead of "Enter value Barney". Each test engineer will choose its own value when going over the test
            case. You may also want to use online services for that like
            <a href="http://textmechanic.com/Random-String-Generator.html">Text Mechanic</a> to help you out.</li>
          <li><b>Randomizing behaviour</b>. Instead of describing every step of the test case in details you
            can choose something more generic. E.g. instead of saying "Press login button, enter your credentials"
            you can say "Sign In". Another popular technique is using checklists instead of test cases. So instead of
            tedious test case with several steps we just say "Check first name is saving with happy path value
            (alphanumeric from 3 to 29)". This way
            every engineer may choose its own path to get to the destination and accomplish what's intended.</li>
        </ul>

        <h2>To sum up</h2>

        <p>Randomized testing is helpful because:</p>
        <ul>
          <li>Provides better
            <a target="_blank" href="https://en.wikipedia.org/wiki/Code_coverage#Parameter_value_coverage">parameter coverage</a></li>
          <li>Helps us finding the cases we didn't think of. This pushes us to think about our domain deeper.</li>
          <li>Isolates the tests so that they don't step on each other toes</li>
          <li>Allows to test large number of combinations in practical time frame</li>
        </ul>
        <p>Does it makes sense using both hardcoded and randomized data? Well, when testing looks complicated
          (Property Based) it makes sense to have examples as well to keep the tests understandable. But apart from
          that there don't seem to be any reasons to do that.</p>
        <p>Note, that randomizing data is just one advice for proper test data management, see
          <a href="test-data-management.html">Effective Data Management</a> for more. Also, check
          out the <a href="test-pyramid.html">Test Pyramid</a> as an example of project where randomized testing is
          used.</p>

      </div>
      <!-- /.blog-post -->
    </div>
    <!-- /.blog-main -->
  </div>
  <!-- /.row -->
</div>


<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "BlogPosting",
      "headline": "Randomized Testing - What, Why, When? | Qala",
      "alternativeHeadline": "Randomize Tests for better coverage | Qala",
      "datePublished": "2015-11-07T00:00:00+00:00",
      "description": "Use randomization in software testing to improve coverage and provide better test isolation."
    }
</script>
