if(!require(ggplot2)) install.packages('ggplot2')

plot_theme = theme(axis.text.x = element_blank(), axis.text.y = element_blank(), legend.position="none")
plot_colors = scale_color_manual(values=c("#00BFC4", "#F8766D"))

people = seq(0, 1000)
performance = people^(1/4)
df = data.frame(people, performance)
names(df) = c("x", "y")
ggplot(mapping=aes(x=df$x, y = df$y)) + 
  geom_line() + geom_area(fill = "#00BFC4") +
  ylab('Team Performance') + xlab('Team size') + plot_theme + plot_colors

people = seq(0, 190)
performance = sin(people / 57)
df = data.frame(people, performance)
names(df) = c("x", "y")
df$color = as.factor(performance >= 0)
ggplot(data = df, mapping = aes(x = df$x, y = df$y)) + 
  geom_line() +  geom_area(mapping = aes(fill = df$color)) +
  ylab('Team Performance') + xlab('Team size') + plot_theme+ plot_colors

# Maintenance costs vs. new features
n_features = seq(0, 100)
maintenance = .3 * n_features
useful_work = 100 - maintenance
df = data.frame(n_features, maintenance)
df2 = data.frame(n_features, 100)
names(df) = c("x", "y")
names(df2) = c("x", "y")
ggplot(data = df, mapping = aes(x = df$x, y = df$y)) + 
  geom_area(mapping = aes(fill = '#F8766D', x = df2$x, y=df2$y), data = df2) +
  geom_line() + geom_area(mapping = aes(fill = '#00BFC4')) +
  ylab('Project Activity') + xlab('Time') +coord_cartesian(ylim = c(0, 100)) + 
  theme(axis.text.x = element_blank(), axis.text.y = element_blank(), legend.title = element_blank()) +
  scale_fill_manual(values=c("#F8766D", "#00BFC4"), labels=c("Maintenance", "Useful work"))
