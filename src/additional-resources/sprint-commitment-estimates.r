if(!require(ggplot2)) install.packages('ggplot2')

sigma = .7
mu = 2
x = seq(-40, 60, length.out = 500)
Z = dlnorm(x, mu, sigma)
data = x
dat = data.frame(x=x, y=Z)

safetyNet = .5
percentile = quantile(data, safetyNet)
firsthalf = subset(dat, x < percentile)
secondhalf = subset(dat, x >= percentile)
ggplot(mapping=aes(x=dat$x, y = dat$y)) + geom_line(data = dat, aes(x = dat$x, y = dat$y)) + 
  geom_area(mapping = aes(x=firsthalf$x, y=firsthalf$y), data = firsthalf, fill = 'steelblue', alpha = 0.3) +
  geom_area(mapping = aes(x=secondhalf$x, y=secondhalf$y), data = secondhalf, fill = 'yellow', alpha = 0.3) +
  geom_vline(color = 'red', xintercept = percentile) + 
  geom_text(aes(label=c('Our estimate')), x = percentile - 1.5, y = 0.03, angle=90) +
  ylab('Odds of finishing the task') + xlab('Time') +
  scale_x_continuous(breaks = seq(0, 40, by = 10), limits = c(0, 40))


dat$cumulative = cumsum(dat$y)
firsthalf = subset(dat, x < percentile)
secondhalf = subset(dat, x >= percentile)

ggplot(mapping=aes(x=dat$x, y = dat$cumulative)) + 
  geom_line(data = dat, aes(x = dat$x, y = dat$cumulative)) +
  geom_area(mapping = aes(x=firsthalf$x, y=cumsum(firsthalf$y)), data = firsthalf, fill = 'steelblue', alpha = 0.3) +
  geom_area(mapping = aes(x=secondhalf$x, y=secondhalf$cumulative), data = secondhalf, fill = 'yellow', alpha = 0.3) +
  scale_x_continuous(breaks = seq(0, 40, by = 10), limits = c(0, 40)) +
  geom_vline(color = 'red', xintercept = percentile) + 
  geom_text(aes(label=c('Our estimate')), x = percentile - 1.5, y = 1, angle=90) +
  ylab('Odds of finishing the task by that time') + xlab('Time')