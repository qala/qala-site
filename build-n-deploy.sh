#!/usr/bin/env bash
set -e

if [ -d 'out' ]; then
  rm -r out/
fi
node_modules/.bin/docpad generate --env static;
tar cvfz qala-site.tgz out;
scp qala-site.tgz qala@jtalks.org:~/;
ssh qala@jtalks -C \
   "tar xvf qala-site.tgz \
 && rm qala-site.tgz \
 && rm -rf qala-site-bkp \
 && mkdir -p www/qala-site \
 && (mv www/qala-site qala-site-bkp) \
 && mv out www/qala-site";

rm qala-site.tgz
